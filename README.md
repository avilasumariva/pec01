# PEC01 - UOC - Un juego de aventuras

El proyecto contiene el directorio de un proyecto de Unity.

La mecánica central del juego desarrollado se basa en responder al improperio del jugador contrario con la respuesta adecuada, si se acierta, se obtiene un punto, si falla, el punto lo consigue el adversario.

A continuación expondremos las particularidades de la implementación de las tres escenas que componene el trabajo.

## Main menu scene

La totalidad de la funcionalidad de esta escena se encuentra en el script MainMenuManager, componente del Panel Menu, que controla la actuación de los métodos referidos a la gestión de escenas, que posteriormente se vinculan a los botones expuestos.

```c#
public class MainMenuManager: MonoBehaviour
{

    public void OnStartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void OnQuit()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }

    public void OnBackToMenu()
    {
        SceneManager.LoadScene(0);
    }
```

Nota.
El contenedor de los botones, los organiza mediante el componente VerticalLayoutGroup.
El clip de audio se reproduce en bucle al iniciar la escena y está inscrita en su AudioMixer como BackgroundMusic, esto mismo sucede en las demás escenas. La implementación de distintas pistas de audio y effectos, además de opciones de, por ejemplo, volumen, se pueden implementar a partir de este punto.


## Game scene

El juego principal.
Dado que, en este caso, el juego se resuelve mediante la interfaz de usuario, los componentes principales se encuentran en el Canvas de la escena, dividido principalmente en tres secciones: el panel del jugador, el dialogo o historial, y panel de la CPU.

P1 Panel.

Aqui se encuentra la representación gráfica del jugador y la sección en la que se seleccionaran las respuestas.

Diálogo.
En este caso, me planteé un log de la conversación. Es donde las interacciones entre los jugadores se dan lugar graficamente, con distintos bocadillos para respuestas o insultos.

P2Panel.
En un principio pretendí implementar un segundo jugador humano, pero dado los requisitos del prototipo, dejé unicamente el sprite representante de la CPU. El espacio liberado se ocupa con un panel de puntuación.

Nota.
Los bocadillos y las reacciones de los personajes cambian según sean respuestas o insultos.

El componente mas importante de esta escena es el script GameManager.

## GameManager
El controlador del juego.
Se inicializa como Singleton para un acceso eficiente desde todos los scripts (Necesario para las respuestas del jugador).

```c#
#region Singleton
public static GameManager instance;
private void Awake()
{
    if(instance == null)
    {
        instance = this;
    }
    else
    {
        Debug.LogError("GameManager is already instantiated.");
    }
}
#endregion

```

En 'Start' el script inicializa la lista de respuestas e insultos disponible. Para ello se usa el script DialogueData, adjunto al script. Este script devuelve una lista de Sentence, la estructura de datos utilizada en este prototipo, que consta de la frase en sí, y un índice para reconocer la relación entre insulto-respuesta.

```c#
[System.Serializable]
public class Sentence
{
    public string Text;
    public int Index;
}
```

Nota:
DialogueData está construido para implementar facilmente la lectura desde ficheros json, que, desafortunadamente en este caso, y tras muchas pruebas, se queda como propuesta de mejora.

El sistema de estados está estructurado de la siguiente manera:
![](https://gitlab.com/avilasumariva/pec01/-/raw/main/scenes.png)

Está implementado como corutinas y como sistema de control utiliza un enumerador pero... el tiempo se me echaba encima :(

  ####BeginGame
  Elige aleatoriamente quien empieza la partida primero, según, pasa al estado de insultar del jugador o de la cpu.

  ####PlayerInsult y PlayerReply
  Rellena el panel de opciones con los insultos disponibles. La responsabilidad de continuar el juego la tiene ahora las opciones con el método onClick del prefab ChatSlot, que vuelve a llamar al método Choice para comprobar el resultado de la elección.
  Las opciones se seleccionan en principio con el cursor.

```c#

  public void Choice(Sentence sentence)
     {
         if (actualPlayer)
         {
             switch (_currentState)
             {
                 case GameState.PLAYER_INSULT:
                     _playerInsult = sentence;

                     ui.AddInsult(sentence.Text, _playerColor);
                     ui.RefreshPlayer(_playerInsultSprite,true);
                     ui.RefreshPlayer(_cpuHitSprite, false);

                     actualPlayer = !actualPlayer;
                     _currentState = GameState.CPU_REPLY;
                     StartCoroutine(CPUReply());

                     break;

                 case GameState.PLAYER_REPLY:
                     _playerReply = sentence;
                     ui.AddReply(sentence.Text, true, _playerColor);

                     StartCoroutine(ResolveGame());
                     break;

                 default:
                     Debug.LogError("Choice failed - No valid state");
                     break;

             }

             ui.DeleteSelection();
         }
         else
         {
             Debug.LogError("Choice failed");
         }
     }
```
 ### CPUInsult y CPUReply
 Según el caso, se elegirá una frase aleatoria de las respectivas listas de insultos o de respuestas, y pasa al siguiente estado.

 Si el estado es de REPLY, para ambos jugadores, el siguiente estado será:

 ### ResolveGame
 Aqui se comprueba quien gana un punto en esa ronda.
 Si alguno de los jugadores llega a la puntuacion de victoria (victoryCondition) se pasa al estado fin, si no, se vuelve a la etapa de "insulto".

 #### GameEnd
 Actualiza con el ganador la variable estática "victory" y se pasa a la ultima escena ENDGAME.

 Nota: He implementado un pequeño menú de pausa. Pulsando la tecla "P" se despliega y se oculta enmedio del juego, aunque no detiene el proceso.


 ### EndGame Scene
 Pantalla final, se despliega un menú con el que puedes rejugar la escena anterior, volver al menú principal o salir de la aplicación.




##WEBGL Export
He usado el paquete WebGL Publisher de Unity para exportar el proyecto a la UnityPlay, a continuación dejo el link desde el cuál se puede jugar al prototipo.
https://play.unity.com/mg/other/build-ita

##Video adjunto
El video capturando la ejecución de una build para pc.
https://youtu.be/e42t1MssZ5k


##Conclusiones
El trabajo ha sido interesante, aunque no haya podido implementar correctamente los apartados técnicos más avanzados: la máquina de estados y la lectura de ficheros, reconozco que son herramientas necesarias para el desarrollo profesional de un sistema como el videojuego, y las investigaciones realizadas en el proceso del proyecto serán muy útiles en cuanto a estructuración mental del diseño de mecánicas.
