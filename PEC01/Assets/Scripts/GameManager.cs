using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


//Main game mechanics methods
public class GameManager : MonoBehaviour
{
    private UIManager ui;
    private List<Sentence> insultsList;
    private List<Sentence> replyesList;

    [SerializeField]
    private DialogueData dialogueData;

    [Header("ViewGameState")]
    [SerializeField]
    private GameState _currentState;
    [SerializeField]
    private bool actualPlayer;
    [SerializeField]
    private int _playerPoints, _CPUPoints;
    [SerializeField]
    private Sentence _playerInsult, _playerReply,_cpuInsult, _cpuReply;

    [Header("Custom Graphics Resources")]

    [SerializeField]
    private Color _playerColor;
    [SerializeField]
    private Color _cpuColor;
    [SerializeField]
    private Sprite _cpuHitSprite, _cpuInsultSprite, _cpuWaitSprite, _cpuWonSprite;
    [SerializeField]
    private Sprite _playerHitSprite, _playerInsultSprite, _playerWaitSprite, _playerWonSprite;

    [Header("Custom Victory Condition")]
    [SerializeField]
    private int _victoryCondition = 3;
    public static bool victory;

    //It establishes a single game manager
    #region Singleton

    public static GameManager instance;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("GameManager is already instantiated.");
        }
    }


    #endregion

    // Start is called before the first frame update
    void Start()
    {
        insultsList = dialogueData.GetInsults();
        replyesList = dialogueData.GetReplys();

        ui = UIManager.instance;

        _currentState = GameState.GAME_START;

       //Go first game state
        BeginGame();

    }

   
    //METHODS

    //Choose first player randomly
    private void RandomPlayer()
    {
        actualPlayer = Random.Range(0,2) == 1;
    }
    
    //Restart the parameters for a new game
    private void ClearGame()
    {
        _playerInsult = null;
        _playerReply = null;
        _cpuInsult = null;
        _cpuReply = null;
    }

    public bool GetActualPlayer()
    {
        return actualPlayer;
    }

    //Resolve player choice
    public void Choice(Sentence sentence)
    {
        if (actualPlayer)
        {
            switch (_currentState)
            {
                case GameState.PLAYER_INSULT:
                    _playerInsult = sentence;

                    ui.AddInsult(sentence.Text, _playerColor);
                    ui.RefreshPlayer(_playerInsultSprite,true);
                    ui.RefreshPlayer(_cpuHitSprite, false);
                    
                    actualPlayer = !actualPlayer;
                    _currentState = GameState.CPU_REPLY;
                    StartCoroutine(CPUReply());

                    break;

                case GameState.PLAYER_REPLY:
                    _playerReply = sentence;
                    ui.AddReply(sentence.Text, true, _playerColor);

                    StartCoroutine(ResolveGame());
                    break;

                default:
                    Debug.LogError("Choice failed - No valid state");
                    break;

            }

            ui.DeleteSelection();
        }
        else
        {
            Debug.LogError("Choice failed");
        }
    }

    //Check if someone won the game
    private bool CheckEnd()
    {

        if (_playerPoints >= _victoryCondition || _CPUPoints >= _victoryCondition)
        {
            victory = actualPlayer;
            _currentState = GameState.GAME_END;
            StartCoroutine(GameEnd());
            return true;
        }
        else
        {
            return false;
        }
    }


    //ROUTINES: States of the game

    public void BeginGame()
    {
        if (_currentState != GameState.GAME_START) return;

        Debug.Log("Comiensan los juegos");
        _playerPoints = 0;
        _CPUPoints = 0;
        ui.RefreshPuntuation(_playerPoints, _CPUPoints);

        ui.ApplyPlayerColor(_playerColor,_cpuColor);


        RandomPlayer();

        if (actualPlayer)
        {
            _currentState = GameState.PLAYER_INSULT;
            Debug.Log("Player time");
            //ui.RefreshPlayer(_playerWaitSprite, true);
            StartCoroutine(PlayerInsult());
        }
        else
        {
            Debug.Log("CPUTime");
            _currentState = GameState.CPU_INSULT;
            //ui.RefreshPlayer(_cpuWaitSprite, false);
            StartCoroutine(CPUInsult());
        }
    }

    public IEnumerator PlayerInsult()
    {
        if (_currentState != GameState.PLAYER_INSULT) Debug.LogError("Fallo en player insult");
        Debug.Log("Player insult");
        Debug.Log("Choose Insult");
       
        ui.RefreshPlayer(_playerWaitSprite, true);
        yield return new WaitForSeconds(1);
        
        UIManager.instance.FillSelection(insultsList);
    }

    public IEnumerator PlayerReply()
    {
        Debug.Log("Player reply");
        ui.RefreshPlayer(_playerWaitSprite, true);
        yield return new WaitForSeconds(1);
        UIManager.instance.FillSelection(replyesList);
    }

    public IEnumerator CPUInsult()
    {
        Debug.Log("CPU insult");
        ui.RefreshPlayer(_cpuWaitSprite, false);
        
        //Random insult from cpu
        Sentence cpuInsult = insultsList[Random.Range(0, insultsList.Count - 1)];
        _cpuInsult = cpuInsult;

        yield return new WaitForSeconds(2);

        ui.RefreshPlayer(_cpuInsultSprite, false);
        ui.RefreshPlayer(_playerHitSprite, true);
        ui.AddInsult(cpuInsult.Text, _cpuColor);
        yield return new WaitForSeconds(2);
        actualPlayer = !actualPlayer;
        _currentState = GameState.PLAYER_REPLY;

        StartCoroutine(PlayerReply());
    }

    public IEnumerator CPUReply()
    {
        yield return new WaitForSeconds(1);

        Debug.Log("CPU REPLY");
        Sentence cpuReply = replyesList[Random.Range(0, replyesList.Count - 1)];

        _cpuReply = cpuReply;
        ui.RefreshPlayer(_cpuWaitSprite, false);

        yield return new WaitForSeconds(1);
        ui.AddReply(cpuReply.Text, false, _cpuColor);

        StartCoroutine(ResolveGame());

    }

    //Resolve who win that play, checking the replyes
    public IEnumerator ResolveGame()
    {
        
        switch (_currentState)
        {

            case GameState.PLAYER_REPLY:

                if(_cpuInsult.Index == _playerReply.Index)
                {
                    _playerPoints++;
                    actualPlayer = true;

                    if (!CheckEnd())
                    {
                        ClearGame();
                        _currentState = GameState.PLAYER_INSULT;
                        StartCoroutine(PlayerInsult());

                    }


                    ui.RefreshPlayer(_playerWonSprite, true);
                    ui.RefreshPlayer(_cpuHitSprite, false);
                    yield return new WaitForSeconds(1);
                }
                else
                {

                    _CPUPoints++;
                    actualPlayer = false;
                    if (!CheckEnd())
                    {
                        ClearGame();
                        _currentState = GameState.CPU_INSULT;
                        StartCoroutine(CPUInsult());
                    }

                    

                    Debug.Log("CPU WIN");
                    Debug.Log("CPU points" + _CPUPoints);

                    ui.RefreshPlayer(_cpuWonSprite, false);
                    ui.RefreshPlayer(_playerHitSprite, true);

                    yield return new WaitForSeconds(1);

                }

                break;
            
            case GameState.CPU_REPLY:

                if(_playerInsult.Index == _cpuReply.Index)
                {
                    _CPUPoints++;
                    actualPlayer = false;
                    if (!CheckEnd())
                    {
                        ClearGame();
                        _currentState = GameState.CPU_INSULT;
                        StartCoroutine(CPUInsult());
                    }


                }
                else
                {
                    _playerPoints++;
                    actualPlayer = true;

                    if (!CheckEnd())
                    {
                        ClearGame();
                        _currentState = GameState.PLAYER_INSULT;
                        StartCoroutine(PlayerInsult());

                    }
                }

                break;
            default:
                Debug.LogError("Resolve error");
                break;
        }

        ui.RefreshPuntuation(_playerPoints, _CPUPoints);
        yield return new WaitForSeconds(1);

    }

   
    //End game state and go end scene
    public IEnumerator GameEnd()
    {
        if (_currentState != GameState.GAME_END) Debug.LogError("Game end error");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(2);
    }

}
