using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ChatSlot : MonoBehaviour
{

    private Sentence sentence;
    [SerializeField]
    private TextMeshProUGUI text;
    [SerializeField]
    private Image image;
    

    public void AddSentence(Sentence sentence)
    {
        this.sentence = sentence;

        this.text.text = sentence.Text;

    }

    public void OnEnter()
    {
        Color auxColor;
        ColorUtility.TryParseHtmlString("#FBFF9B", out auxColor);
        image.color = auxColor;

    }

    public void OnExit()
    {
        
        Color auxColor;
        ColorUtility.TryParseHtmlString("#D9DAE7", out auxColor);
        image.color = auxColor;
    }

    public void OnClick()
    {

        GameManager.instance.Choice(sentence);



    }


}
