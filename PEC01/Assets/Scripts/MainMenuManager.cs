using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenuManager: MonoBehaviour
{

    public void OnStartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void OnQuit()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }

    public void OnBackToMenu()
    {
        SceneManager.LoadScene(0);
    }



}



