
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    [Header("Structure")]
    [SerializeField]
    private Transform playerChatContain;
    [SerializeField]
    private Scrollbar dialogueScrollBar;
    [SerializeField]
    private Image _playerImage, _cpuImage;

    [Header("Prefabs")]
    
    [SerializeField]
    private GameObject leftBubble, rightBubble, yellBubble;
    [SerializeField]
    private GameObject senteceSlot;
    [SerializeField]
    private GameObject dialogueContain;

    [Header("Score")]
    [SerializeField]
    private TextMeshProUGUI playerPointsText, cpuPointsText;

    [Header("PauseMenu")]
    [SerializeField]
    private GameObject _pauseScreen;


    //UIController

    #region Singleton
    public static UIManager instance;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("UIManager is already instantiated.");
        }

    }


    #endregion


    void Update()
    {
        //The light pause menu
        if (Input.GetKeyDown(KeyCode.P))
        {
            _pauseScreen.SetActive(!_pauseScreen.activeSelf);
        }
    }

    //put down dialogue scroll
    public void ResetScrollBar()
    {
        dialogueScrollBar.value = 0;
    }


    //Populate the player options panel
    public void FillSelection(List<Sentence> sentences)
    {

        DeleteSelection();
       
        if(sentences.Count > 0)
        {
            foreach (Sentence s in sentences)
            {
                var slot = Instantiate(senteceSlot, transform.position, Quaternion.identity);
                slot.transform.SetParent(playerChatContain.transform, false);
                slot.GetComponent<ChatSlot>().AddSentence(s);
            }
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)playerChatContain.transform);
        }

    }

    public void DeleteSelection()
    {
        foreach (Transform child in playerChatContain.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    //Add elements in the dialogue scroll
    public void AddReply(string text, bool player, Color color)
    {
        if (player)
        {
            var bubble = Instantiate(leftBubble, transform.position, Quaternion.identity);
            bubble.transform.SetParent(dialogueContain.transform, false);

            bubble.GetComponent<BubbleChat>().SetText(text);
            bubble.GetComponent<BubbleChat>().SetColor(color);

        }
        else
        {
            var bubble = Instantiate(rightBubble, transform.position, Quaternion.identity);
            bubble.transform.SetParent(dialogueContain.transform, false);

            bubble.GetComponent<BubbleChat>().SetText(text);
            bubble.GetComponent<BubbleChat>().SetColor(color);
        }



        ResetScrollBar();
    }

    public void AddInsult(string text, Color color)
    {
        var bubble = Instantiate(yellBubble, transform.position, Quaternion.identity);
        bubble.transform.SetParent(dialogueContain.transform, false);

        bubble.GetComponent<BubbleChat>().SetText(text);
        bubble.GetComponent<BubbleChat>().SetColor(color);

        ResetScrollBar();
    }

    //actualize the game score
    public void RefreshPuntuation(int playerPoints, int cpuPoints)
    {
        playerPointsText.text = playerPoints.ToString();
        cpuPointsText.text = cpuPoints.ToString();
    }


    public void RefreshPlayer(Sprite img, bool player)
    {
        if (player)
        {
            _playerImage.sprite = img;
        }
        else
        {
            _cpuImage.sprite = img;
        }
    }

    public void ApplyPlayerColor(Color playerColor, Color cpuColor)
    {

        _playerImage.color = playerColor;
        _cpuImage.color = cpuColor;

    }



}
