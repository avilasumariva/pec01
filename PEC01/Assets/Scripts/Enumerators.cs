enum GameState { 
    GAME_START,
    PLAYER_INSULT,
    PLAYER_REPLY,
    CPU_INSULT,
    CPU_REPLY,
    RESOLVE,
    GAME_END
};

