using System.Collections.Generic;
using System.IO;
using UnityEngine;

//Sentences construction manager.

public class DialogueData : MonoBehaviour
{
    private List<Sentence> insultsList;
    private List<Sentence> replyesList;

    private void Awake()
    { 
        insultsList = InsultsList();
        replyesList = ReplyesList();
    }

    //Generate the list of insults, can be replaced by a json reader.
    public List<Sentence> InsultsList()
    {
        List<Sentence> insults = new List<Sentence>();

        var insult01 = CreateSentence(
            "�Has dejado ya de usar pa�ales?", 1
            );

        var insult02 = CreateSentence(
            "Espero que tengas un barco para una r�pida huida.", 1
            );

        var insult03 = CreateSentence(
            "�No hay palabras para describir lo asqueroso que eres!", 2
            );

        var insult04 = CreateSentence(
            "Ya no hay t�cnicas que te puedan salvar.", 2
            );

        var insult05 = CreateSentence(
            "�He hablado con simios m�s educados que tu!", 3
            );

        var insult06 = CreateSentence(
            "Ahora entiendo lo que significan basura y estupidez.", 3
            );

        var insult07 = CreateSentence(
            "�Llevar�s mi espada como si fueras un pincho moruno! ", 4
            );

        var insult08 = CreateSentence(
            "Mi lengua es m�s h�bil que cualquier espada.", 4
            );

        var insult09 = CreateSentence(
            "�Luchas como un ganadero!", 5
            );

        var insult10 = CreateSentence(
            "�Orde�ar� hasta la �ltima gota de sangre de tu cuerpo!", 5
            );

        var insult11 = CreateSentence(
            "�Mi pa�uelo limpiar� tu sangre!", 6
            );

        var insult12 = CreateSentence(
            "�Una vez tuve un perro m�s listo que tu!", 7
            );

        var insult13 = CreateSentence(
            "S�lo he conocido a uno tan cobarde como t�.", 7
            );
        insults.Add(insult01);
        insults.Add(insult02);
        insults.Add(insult03);
        insults.Add(insult04);
        insults.Add(insult05);
        insults.Add(insult06);
        insults.Add(insult07);
        insults.Add(insult08);
        insults.Add(insult09);
        insults.Add(insult10);
        insults.Add(insult11);
        insults.Add(insult12);        
        insults.Add(insult13);

        return insults;
    }
    //Generate the list of replys, can be replacer by a json reader
    public List<Sentence> ReplyesList()
    {
        List<Sentence> replyes = new List<Sentence>();

        var reply01 = CreateSentence(
            "�Por qu�? �Acaso quer�as pedir uno prestado?", 1
            );

        var reply02 = CreateSentence(
            "�S� que las hay, s�lo que nunca las has aprendido.", 2
            );

        var reply03 = CreateSentence(
            "Me alegra que asistieras a tu reuni�n familiar diaria.", 3
            );


        var reply04 = CreateSentence(
            "Primero deber�as dejar de usarla como un plumero.", 4
            );

        var reply05 = CreateSentence(
            "Qu� apropiado, t� peleas como una vaca.", 5
            );

        var reply06 = CreateSentence(
            "Ah, �Ya has obtenido ese trabajo de barrendero?", 6
            );

        var reply07 = CreateSentence(
            "Te habr� ense�ado todo lo que sabes.", 7
            );

        replyes.Add(reply01);
        replyes.Add(reply02);
        replyes.Add(reply03);
        replyes.Add(reply04);
        replyes.Add(reply05);
        replyes.Add(reply06);
        replyes.Add(reply07);

        return replyes;
    }



    private Sentence CreateSentence(string text, int index)
    {
        var sentence = new Sentence
        {
            Text = text,
            Index = index

        };
        return sentence;

    }

    public List<Sentence> GetInsults()
    {
        return insultsList;

    }
    public List<Sentence> GetReplys()
    {
        return replyesList;
    }
     




}


