using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class VictoryScreen : MonoBehaviour
{

    [SerializeField]
    private Image _winnerImage;


    [SerializeField]
    private Sprite _playerWonSprite;

    [SerializeField]
    private Sprite _cpuWonSprite;



    private void Start()
    {
        if (GameManager.victory)
        {

            Debug.Log("Player won");
            _winnerImage.sprite = _playerWonSprite;
        }
        else
        {
            Debug.Log("Computer won");
            _winnerImage.sprite = _cpuWonSprite;
        }
    }


}
